/**
 * Action Types
 * @type {string}
 */
export const USERS__GET_USERS = 'USERS__GET_USERS';
export const USERS__SET_ROWS_PER_PAGE = 'USERS__SET_ROWS_PER_PAGE';
export const USERS__SET_PAGE = 'USERS__SET_PAGE';
export const USERS__SET_FILER = 'USERS__SET_FILER';

export const USERS__EDIT_USER = 'USERS_EDIT_USER';

/**
 * Urls
 * @type {string}
 */
export const USERS__GET_USERS_URL = '/employees/employeespage';
export const USERS__EDIT_USER_URL = '/employees/employee';
export const FAIL = 'FAIL';