import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import appReducer from './appReducer';
import authReducer from './authReducer';
import usersReducer from './usersReducer';

export default combineReducers({
  appReducer,
  authReducer,
  usersReducer,
  routerReducer
});
