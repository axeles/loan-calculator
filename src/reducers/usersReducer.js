import * as usersConstants from '../constants/UsersConstants';

const initialState = {
  userList: {
    data: [
      {
        id: 0,
        name: '',
        surname: '',
        patronymic: '',
        passportSeries: '',
        passportNumber: '',
        birthday: '',
        position: '',
        appointmentDate: '',
        location: {
          id: 0,
          address: '',
          locationType: '',
          active: true
        },
        user: {
          id: 0,
          login: '',
          password: '',
          role: '',
          locationId: 0,
          active: true
        },
        active: true
      }
    ],
    total: 0
  },
  page: 0,
  count: 10,
  filter: '',
  error: '',
  user: {
    id: 0,
    name: '',
    surname: '',
    patronymic: '',
    passportSeries: '',
    passportNumber: '',
    birthday: '',
    position: '',
    appointmentDate: '',
    location: {
      id: 0,
      address: '',
      locationType: '',
      active: true
    },
    user: {
      id: 0,
      login: '',
      password: '',
      role: '',
      locationId: 0,
      active: true
    },
    active: true
  }
};

export default function usersReducer(state = initialState, action) {
  const { type, userList, page, count, filter, message } = action;
  switch (type) {
    case usersConstants.USERS__GET_USERS:
      return {
        ...state,
        page,
        count,
        userList,
        filter
      };
    case usersConstants.FAIL:
      return { ...state, error: message.message };
    default:
      return state;
  }
}
