import jwt_decode from 'jwt-decode';


/**
 * from router
 * @param routerReducer
 * @returns {*}
 */
export const getCurrentMatchLocation = ({ routerReducer }) =>
  routerReducer.match;

/**
 * Login
 * @param authReducer
 * @returns {boolean}
 */
export const currentUserState = ({ authReducer }) =>
  authReducer.isAuthenticated;

export const getCurrentUser = () =>
  localStorage.getItem('id_token') !== null &&
  localStorage.getItem('id_token') !== undefined
    ? jwt_decode(localStorage.getItem('id_token'))
    : { authorities: [] };

export const currentUserErrorMessage = ({ authReducer }) =>
  authReducer.errorMessage;

/**
 * Users
 * @param usersReducer
 * @returns {Array}
 */
export const getUserList = ({
  usersReducer: {
    userList: { data }
  }
}) => data;

export const getUserListTotalCount = ({
  usersReducer: {
    userList: { total }
  }
}) => total;

export const getUserListPage = ({ usersReducer }) => usersReducer.page;
export const getUserListCount = ({ usersReducer }) => usersReducer.count;
export const getUserListFilter = ({ usersReducer }) => usersReducer.filter;
export const getSelectedUser = ({ usersReducer }) => usersReducer.user;
