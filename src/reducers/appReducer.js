import * as actionTypes from '../constants/AppConstant';
import modulesList from "../Models/modules";
import classesList from "../Models/classes";

const initialState = {
  showSearchResults: false,
  animationClass: null,
  currentPath: {
    path: '/',
    title: 'home'
  },
  parentPath: {
    path: '/',
    title: 'home'
  },
  backButtonParams: {
    buttonCode: '',
    iconPath: ''
  },
  logoParams: {
    logoImagePath: '/img/neoflex_rus.svg'
  },
  linksList: modulesList.modules,
  linksBlockParams: {
    buttonCode: '',
    buttonIconPath: '',
    header: 'Модули',
    headerIconName: 'chart-bar'
  },
  workbenchParams: {
    listParams: {
      header: 'Packages',
      buttonCode: 'add'
    },
    linksList1: classesList.classes,
    linksBlockParams1: {
      buttonCode: '',
      buttonIconPath: '',
      header: 'Классы',
      headerIconName: 'chalkboard-teacher'
    },
    linksList2: modulesList.modules,
    linksBlockParams2: {
      buttonCode: '',
      buttonIconPath: '',
      header: 'Zeppelin',
      headerIconName: 'balance-scale'
    },
    linksList3: classesList.classes,
    linksBlockParams3: {
      buttonCode: '',
      buttonIconPath: '',
      header: 'Abra-Cadabra',
      headerIconName: 'blind'
    }
  }
};

export default function appReducer(state = initialState, action = {}) {
  const { type } = action;
  switch (type) {
    case actionTypes.APP__SET_ANIMATION_CLASS:
      return {
        ...state,
        animationClass: action.payload
      };
    case actionTypes.APP__SWITCH_LANGUAGE:
      return {
        ...state,
        locale: action.payload
      };
    case actionTypes.APP__LINK_SELECTED:
      return {
        ...state,
        currentPath: action.payload.currentPath,
        parentPath: action.payload.parentPath,
        linksList: action.payload.linksList,
        linksBlockParams: {
          buttonCode: action.payload.linksBlockParams.buttonCode,
          buttonIconPath: action.payload.linksBlockParams.buttonIconPath,
          header: action.payload.linksBlockParams.header,
          headerIconName: action.payload.linksBlockParams.headerIconName
        }
      };
    case actionTypes.APP__BACK_BUTTON_CLICKED:
      return {
        ...state,
        animationClass: action.payload.animationClass,
        currentPath: action.payload.currentPath,
        parentPath: action.payload.parentPath,
        linksList: action.payload.linksList,
        linksBlockParams: {
          buttonCode: action.payload.linksBlockParams.buttonCode,
          buttonIconPath: action.payload.linksBlockParams.buttonIconPath,
          header: action.payload.linksBlockParams.header,
          headerIconName: action.payload.linksBlockParams.headerIconName
        }
      };
    case actionTypes.APP__BACK_BUTTON_CHANGED:
      return {
        ...state,
        backButtonParams: {
          buttonCode: action.payload.buttonCode,
          iconPath: action.payload.iconPath
        }
      };
    case actionTypes.APP__SEARCH_SUCCESS:
      return {
        ...state,
        isFetching: action.isFetching,
        showSearchResults: action.showSearchResults,
        showSearchInSidebar: action.showSearchInSidebar,
        profiles: action.profiles
      };
    case actionTypes.APP__SEARCH_PARAMS:
      return {
        ...state,
        searchParams: action.params
      };
    default:
      return state;
  }
}
