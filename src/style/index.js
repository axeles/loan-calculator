import { createMuiTheme } from '@material-ui/core/styles';

const colors = {
  primary: {
    light: '#feaa91',
    main: '#fd5523',
    dark: '#fc310d',
    contrastText: '#fff',
  },
  secondary: {
    light: '#f5f5f5',
    main: '#c8c8c8',
    dark: '#999',
    contrastText: '#333',
  },
  tertiary: {
    light: '#666',
    main: '#555',
    dark: '#333',
    contrastText: '#999'
  },
  alerts: {
    success: '#6bc40c',
    danger: '#d7151d',
    warning: '#f9aa33'
  }
};
const theme = {
  palette: {
    actions: {
      active: 'rgba(0, 0, 0, 0.54)',
      disabled: 'rgba(0, 0, 0, 0.26)',
      disabledBackground: 'rgba(0, 0, 0, 0.12)',
      hover: 'rgba(0, 0, 0, 0.08)',
      hoverOpacity: '0.08',
      selected:'rgba(0, 0, 0, 0.14)'
    },
    primary: colors.primary,
    secondary: colors.secondary,
    tertiary: colors.tertiary,
    error: {
      light: colors.secondary.light,
      main: colors.secondary.main,
      dark: colors.secondary.dark,
      contrastText: colors.secondary.contrastText
    },
    alerts: colors.alerts
  },
  props: {
    buttonStyle: {
      borderRadius: 0,
      boxShadow: 'none',
      medium: {
        width: 110,
        height: 40
      },
      disabled: {
        color: colors.primary.contrastText,
        backgroundColor: colors.primary.light,
        border: '1px solid',
        borderColor: colors.secondary.light
      }
    },
    linkStyle: {
      color: colors.tertiary.contrastText,
      hoveredLink: colors.secondary.main
    }

  },
  typography: {
    fontSize: 14,
    title: {
      color: '#999',
      fontSize: '1.5rem'
    },
    caption: {
      fontSize: '1rem'
    }
  }
};

export default createMuiTheme(theme);
