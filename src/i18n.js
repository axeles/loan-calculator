import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import ru_common from './i18n/ru/common'
import en_common from './i18n/en/common'
import ru_links from './i18n/ru/links'
import ru_modules from './i18n/ru/modules'
import ru_classes from './i18n/ru/classes'
import ru_references from './i18n/ru/references'
import ru_views from './i18n/ru/views'
import en_links from './i18n/en/links'
import en_modules from './i18n/en/modules'
import en_classes from './i18n/en/classes'
import en_references from './i18n/en/references'
import en_views from './i18n/en/views.json'

i18n
    //.use(XHR)
    // .use(Cache)
    .use(LanguageDetector)
    .init({
        resources: {
            "ru-RU": {
                "links": ru_links,
                "modules": ru_modules,
                "classes": ru_classes,
                "common": ru_common,
                "references": ru_references,
                "views": ru_views
            },            
            "en-EN": {
                "links": en_links, 
                "modules": en_modules, 
                "classes": en_classes, 
                "common": en_common,
                "references": en_references,
                "views": en_views
            }            
        },
        fallbackLng: 'en-EN',

        // have a common namespace used around the full app
        ns: ['common'],
        defaultNS: 'common',

        debug: true,

        // cache: {
        //   enabled: true
        // },

        interpolation: {
            escapeValue: false, // not needed for react!!
            formatSeparator: ',',
            format: function(value, format, lng) {
                if (format === 'uppercase') return value.toUpperCase();
                return value;
            }
        }
    });


export default i18n
