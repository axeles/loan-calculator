import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import theme from './style';

import Routes from './components/Routes';
import createHistory from './store/history';
import configureStore from './store';

const store = configureStore(createHistory);

const render = Component => {
  ReactDOM.render(
    <AppContainer warnings={false}>
      <Provider store={store}>
        <MuiThemeProvider theme={theme}>
          <Component />
        </MuiThemeProvider>
      </Provider>
    </AppContainer>,
    document.querySelector('#root')
  );
};

render(Routes);

if (module.hot) {
  module.hot.accept('./components/Routes', () => {
    // eslint-disable-next-line
    const NextRoutes = require('./components/Routes').default;

    render(NextRoutes);
  });
}
