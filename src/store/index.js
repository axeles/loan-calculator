import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import { routerMiddleware } from 'react-router-redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import history from './history';

import rootReducer from '../reducers/';

const configureStore = () => {
  const router = routerMiddleware(history);

  const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(thunk, router, createLogger()))
  );

  if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept('../reducers', () => {
      // eslint-disable-next-line
      const nextReducer = require('../reducers/').default;

      store.replaceReducer(nextReducer);
    });
  }

  return store;
};

export default configureStore;
