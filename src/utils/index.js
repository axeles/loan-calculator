import * as R from 'ramda';

import FileSaver from 'file-saver';

export const overProp = R.curry((prop, fn, obj) =>
  R.ifElse(
    R.propSatisfies(R.isNil, prop),
    R.identity,
    R.over(R.lensProp(prop), fn)
  )(obj)
);

export const addRemoveItem = R.curry((item, list) => {
  const index = R.findIndex(R.equals(item), list);

  return index > -1
    ? R.concat(
        R.slice(0, index, list),
        R.slice(index + 1, R.length(list), list)
      )
    : R.sort(R.descend(R.identity), R.append(item, list));
});

const ifNil = R.curry((value, obj) =>
  R.ifElse(R.isNil, R.always(value), R.identity)(obj)
);

export const isFirefoxOrIE = () =>
  navigator.userAgent.indexOf('Firefox') !== -1 ||
  navigator.userAgent.indexOf('MSIE') !== -1 ||
  navigator.userAgent.indexOf('Trident/') !== -1 ||
  navigator.userAgent.indexOf('Edge/') !== -1;

function saveFile(data, filename) {
  FileSaver.saveAs(data, filename);
}

export function readStreamAndDownloadToFile(readableStream, filename) {
  let reader;

  if (isFirefoxOrIE) {
    saveFile(readableStream, filename);
  } else {
    reader = readableStream.getReader();
    const chunks = [];

    (function pump() {
      reader.read().then(({ value, done }) => {
        if (done) {
          saveFile(
            new Blob(chunks, { type: 'text/csv;charset=utf-8' }),
            filename
          );
        } else {
          chunks.push(value);
          pump();
        }
      });
    })();
  }
}
export const getReportHeader = (auth = localStorage.getItem('token')) => ({
  Authorization: `Bearer ${auth}`,
  Accept: 'application/xlsx',
  'Content-Type': 'application/xlsx charset=utf-8',
  'Access-Control-Allow-Origin': '*',
  credentials: 'include'
});
export const getHeader = (auth = localStorage.getItem('token')) => ({
  Authorization: `Bearer ${auth}`,
  Accept: 'application/json',
  'Content-Type': 'application/x-www-form-urlencoded charset=utf-8',
  'Access-Control-Allow-Origin': '*'
});

export const getJSONHeader = (auth = localStorage.getItem('token')) => ({
  Authorization: `Bearer ${auth}`,
  Accept: 'application/json',
  'Content-Type': 'application/json charset=utf-8',
  'Access-Control-Allow-Origin': '*'
});
