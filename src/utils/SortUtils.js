import * as R from 'ramda';

const sortOrderFields = {
  mark: '',
  change_time: '',
  change_author: '',
  number: '',
  year: '',
  run: '',
  maintence_l_date: '',
  maintence_n_date: '',
  current_status: '',
  current_status_date: '',
  next_status: '',
  next_status_date: ''
};

const initSortOrderState = R.assoc('mark', 'desc', sortOrderFields);

const updateOrder = R.ifElse(
  R.equals('asc'),
  R.always('desc'),
  R.ifElse(R.equals('desc'), R.always(''), R.always('asc'))
);

const updateSortField = field => sortOrder =>
  R.assoc(field, updateOrder(R.prop(field, sortOrder)), sortOrderFields);

// -----------------------------------
// Apply filter with sort in selector
// -----------------------------------
const getSortOrder = R.ifElse(
  R.compose(
    R.equals('asc'),
    R.last
  ),
  R.compose(
    R.ascend,
    R.prop,
    R.head
  ),
  R.compose(
    R.descend,
    R.prop,
    R.head
  )
);

const getSortFunc = R.compose(
  R.map(getSortOrder),
  R.filter(
    R.compose(
      R.not,
      R.isEmpty,
      R.last
    )
  ),
  R.toPairs
);

// TODO: generalize filter function
// TODO add filter
// const applyHistoryFilterWithSort = (sortOrder, filter, taskList)
const applyHistoryFilterWithSort = (sortOrder, taskList) =>
  R.compose(
    R.sortWith(getSortFunc(sortOrder))
    //
    // R.filter(
    //   R.or(
    //     R.compose(
    //       R.test(new RegExp(filter, 'i')),
    //       R.prop('change_author')
    //     ),
    //     R.compose(
    //       R.test(new RegExp(filter, 'i')),
    //       R.prop('number')
    //     ),
    //     R.compose(
    //       R.test(new RegExp(filter, 'i')),
    //       R.prop('maintence_l_date')
    //     ),
    //     R.compose(
    //       R.test(new RegExp(filter, 'i')),
    //       R.prop('maintence_n_date')
    //     )
    //   )
    // )
  )(taskList);

const filterAutoList = (filter, list) => {
  return list.length > 0
    ? R.filter(
        val =>
          R.or(
            R.test(new RegExp(filter, 'i'), val.number),
            R.test(new RegExp(filter, 'i'), val.mark),
            R.test(new RegExp(filter, 'i'), val.year),
            R.test(new RegExp(filter, 'i'), val.mileage),
            R.test(new RegExp(filter, 'i'), val.currentStatus),
            R.test(new RegExp(filter, 'i'), val.nextStatus)
          ),
        list
      )
    : [];
};
const filterHistoryList = (filter, list) => {
  return list.length > 0
    ? R.filter(val => R.test(new RegExp(filter, 'i'), val.number), list)
    : [];
};

export {
  initSortOrderState,
  updateSortField,
  applyHistoryFilterWithSort,
  filterAutoList,
  filterHistoryList
};
