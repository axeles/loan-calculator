import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { SmallButton, Logo } from 'flexcom';
import FlexWrapper from '../FlexWrapper/FlexWrapper';
import Login from '../Login/Login';
import Grid from '../FlexGrid/FlexGrid';
import LeftBar from '../FlexLeftBar/FlexLeftBar';
import NavBlock from '../NavBlock/NavBlock'
import Dashboard from '../Dashboard/Dashboard'

import i18n from '../../i18n';
import * as appActions from '../../actions/appActions';
import * as authActions from '../../actions/authActions';
import Route from "react-router-dom/es/Route";

const styles = theme => ({
  root: {
  },
  langSwitcher: {
    position: 'absolute',
    top: '.2rem',
    right: '.2rem'
  },
  logo: {
    margin: '0 0 0 auto'
  },
  appearFromRightAnimation: {
    animation: 'appearFromRight .2s ease-out',
    animationFillMode: 'backwards'
  },
  '@keyframes appearFromRight': {
    '0%': {
      opacity: 0,
      transform: 'translate(200px)',
    },
    '100%': {
      opacity: 1,
      transform: 'translateX(0)'
    }
  }
});

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showRightBar: false
    }
    // this.props.setAnimationClass(this.props.classes.appearFromRightAnimation);
    // console.log('+++ [App] props:', this.props);
    // console.log('+++ [App] theme', createMuiTheme());
  }

  onLogoClick = () => {
    this.setState({showRightBar: false}, () =>
      window.setTimeout(this.props.history.push, 300, '/')
    );
  };

  onForgotPassword = () => {
    this.setState({showRightBar: true})
  };

  onLinkSelected = (link) => {
    console.log('+++ [App] onLinkSelected()', link)
  };

  getLangSwitcherLabel = (locale) => {
    let label = 'EN';
    switch(locale) {
      case 'ru-RU':
        label = 'EN';
        break;
      case 'en-EN':
        label = 'RU';
        break;
      default:
        label = 'RU';
    }
    return label;
  };

  switchLang = () => {
    const locale = i18n.language;
    let localeToSwitch = 'en-EN';
    switch (locale) {
      case 'ru-RU':
        localeToSwitch ='en-EN';
        break;
      case 'en-EN':
        localeToSwitch ='ru-RU';
        break;
      default:
        localeToSwitch ='ru-RU';
    }
    i18n.changeLanguage(localeToSwitch);
    this.props.switchLanguage(localeToSwitch);
  };

  prepareLeftBar = () => {
    const { classes } = this.props;
    const { animationClass, linksList, logoParams } = this.props.app;
    return (
      <LeftBar
        title={{name: 'Кредитный калькулятор', description: 'потребительский кредит'}}
      >
        <Logo
          classes={{root: classes.logo}}
          params={logoParams}
          onLogoClick={this.onLogoClick}
        />
        <NavBlock
          classes={{container: animationClass}}
          linksList={linksList}
          onLinkSelected={this.onLinkSelected}
        />
      </LeftBar>
    );
  };

  prepareBody = () => {
    const { classes } = this.props;
    const { animationClass } = this.props.app;
    const errorMessage = ''; //'Неверное имя пользователя или пароль';
    return (
      <Login
        className={classes && classes.appearFromRightAnimation}
        classes={{ root: animationClass }}
        loginUser={(user) => console.log('+++ user:', user)}
        i18n={i18n}
        forgotPasswordLinkPath={'/forgot-password'}
        onForgotPassword={this.onForgotPassword}
        errorMessage={errorMessage}
      />
    );
  };

  prepareRightBar = () => {
    return (
      <Route path="/forgot-password" component={Dashboard}/>
    );
  };

  render() {
    const { classes } = this.props;

    return (
      <FlexWrapper>
        <SmallButton
          classes={{root: classes.langSwitcher}}
          label={this.getLangSwitcherLabel(i18n.language)}
          onClick={this.switchLang}
        />

        <Grid
          LeftBar={this.prepareLeftBar}
          Body={this.prepareBody}
          RightBar={this.prepareRightBar}
          showRightBar={this.state.showRightBar}
        />
      </FlexWrapper>
    )
  }
}

const mapStateToProps = state => {
  return {
    app: state.appReducer,
    auth: state.authReducer,
    user: state.usersReducer,
    router: state.routerReducer
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginUser: credentials => dispatch(authActions.loginUser(credentials)),
    logoutUser: () => dispatch(authActions.logoutUser()),
    setAnimationClass: payload => dispatch(appActions.setAnimationClass(payload)),
    switchLanguage: payload => dispatch(appActions.switchLanguage(payload)),
    linkSelected: payload => dispatch(appActions.linkSelected(payload)),
    backButtonClicked: payload => dispatch(appActions.backButtonClicked(payload)),
    backButtonChanged: payload => dispatch(appActions.backButtonChanged(payload)),
    getProfileAll: params => dispatch(appActions.getProfileAll(params)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(withRouter(App)));
