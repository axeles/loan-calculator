import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";
import className from 'classname';

const styles = theme => ({
  container: {
    display: 'flex',
    justifyContent: 'flex-start',
    width: '100%',
    maxWidth: '1600px',
    margin: '0 auto',
    fontSize: 'inherit',
    fontFamily: 'inherit',
    // backgroundColor: theme.palette.secondary.light,
    height: '100vh',
    overflow: 'hidden'
  },
  leftBar: {
    width: '33%',
    // backgroundColor: theme.palette.secondary.main
  },
  bodyNarrow: {
    width: '33%',
    // backgroundColor: theme.palette.secondary.light
  },
  bodyWide: {
    width: '67%',
    // backgroundColor: theme.palette.secondary.light
  },
  rightBarWide: {
    width: '34%',
    // backgroundColor: theme.palette.secondary.light
  },
  rightBarNarrow: {
    width: '0%',
    // backgroundColor: theme.palette.secondary.light
  },
  widen: {
    transition: 'width 300ms ease-out',
  },
  appear: {
    animation: 'appear .3s ease-out',
    animationFillMode: 'backwards'
  },
  disappear: {
    animation: 'disappear .3s ease-out',
    animationFillMode: 'backwards'
  },
  '@keyframes appear': {
    '0%': {
      opacity: 0,
      transform: 'translate(-100%)',
    },
    '100%': {
      opacity: 1,
      transform: 'translateX(0)'
    },
  },
  '@keyframes disappear': {
    '0%': {
      opacity: 1,
      transform: 'translate(0)',
    },
    '100%': {
      opacity: 0,
      transform: 'translateX(-100%)'
    }
  }
});

class FlexGrid extends Component {
  render() {
    const { classes, LeftBar, Body, RightBar, showRightBar } = this.props;
    // console.log('+++ [FlexLeftBar] RightBar', RightBar());
    const bodyStyle = showRightBar ? className(classes.widen, classes.bodyNarrow) : className( classes.widen, classes.bodyWide);
    const rightBarStyle = showRightBar ? className(classes.widen, classes.rightBarWide ) : className(classes.widen,classes.rightBarNarrow);

    return (
      <div className={classes.container}>
        <div className={classes.leftBar}><LeftBar /></div>
        <div className={bodyStyle}><Body /></div>
        <div className={rightBarStyle}><RightBar /></div>
      </div>
    )
  }
}

FlexGrid.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.object,
  showRightBar: PropTypes.bool.isRequired,
  LeftBar: PropTypes.func.isRequired,
  Body: PropTypes.func.isRequired,
  RightBar: PropTypes.func
};

export default withStyles(styles)(FlexGrid);
