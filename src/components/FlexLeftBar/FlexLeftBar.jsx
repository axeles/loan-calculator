import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    fontSize: 'inherit',
    fontFamily: 'inherit',
    backgroundColor: theme.palette.secondary.light,
    height: '100%'
  },
  content: {
    display: 'flex',
    position: 'relative',
  },
  sidebar: {
    flex: 1
  },
  titleContainer: {
    position: 'absolute',
    top: '10rem',
    left: '-10rem',
    display: 'flex',
    flex: '1',
    lineHeight: '1.5rem',
    // justifyContent: 'space-between',
    transform: 'rotate(-90deg)',
    alignSelf: 'stretch',
    // margin: 'auto 0 0 0',
    width: '30rem'
    // marginTop: '10rem'
  },
  title: {
    display: 'flex',
    flexShrink: '1',
    flex: '3',
    flexDirection: 'column',
    textAlign: 'right',
    height: '6rem',
    width: '20rem'
  },
  titleName: {
    fontSize: theme.typography.caption.fontSize,
    fontWeight: theme.typography.caption.fontWeight,
    fontFamily: theme.typography.caption.fontFamily,
    textTransform: 'uppercase',
    width: '100%'
  },
  titleDescription: {
    fontSize: '.815rem',
    color: theme.palette.primary.main,
    width: '100%'
  },
  titleDecorator: {
    flex: '1',
    backgroundColor: theme.palette.tertiary && theme.palette.tertiary.contrastText,
    height: '2px',
    width: '10rem',
    marginTop: '2.1rem',
    marginLeft: '1.4rem',
    flexGrow: 1,
    alignSelf: 'flex-start'
  },
  body: {
    flex: 3,
    // alignItems: 'flex-start',
    marginRight: '3rem',
    // textAlign: 'right',
    // justifyContent: 'flex-end'
  },
  footer: {
    display: 'flex',
    justifyContent: 'space-between',
    backgroundColor: theme.palette.tertiary && theme.palette.tertiary.dark,
    height: '10rem'
  },
  footerButton: {
    margin: '48px'
  },
  footerBody: {
    fontSize: '1.5rem',
    color: theme.palette.primary.main,
    textAlign: 'right',
    textTransform: 'uppercase',
    margin: '48px',
  }
});

class FlexLeftBar extends Component {
  render() {
    const { classes, children, title } = this.props;

    return (
      <div className={classes && classes.root}>
        <div className={classes && classes.content}>
          <div className={classes && classes.sidebar}>
          <div className={classes && classes.titleContainer}>
            <div className={classes && classes.title}>
              <div className={classes && classes.titleDescription}>{title.description}</div>
              <div className={classes && classes.titleName}>{title.name}</div>
            </div>
            <div className={classes && classes.titleDecorator}>&nbsp;</div>
          </div>
          </div>
          <div className={classes && classes.body}>{children}</div>
        </div>
        <div className={classes && classes.footer}>
          <div className={classes && classes.footerButton}><img src="img/footer-arrow.svg" alt="back-button"/></div>
          <div className={classes && classes.footerBody}>Новый расчет</div>
        </div>
      </div>
    )
  }
}

FlexLeftBar.propTypes = {
  title: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
  }).isRequired,
  classes: PropTypes.object.isRequired,
  children: PropTypes.array
};

export default withStyles(styles)(FlexLeftBar);
