import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";

const styles = theme => ({
  root: {
    display: 'grid',
    gridTemplateColumns: '66% calc(34% - 1rem)',
    gridGap: '1rem 1rem',
    fontSize: 'inherit',
    fontFamily: 'inherit'
  }
});

class Grid_2_1 extends Component {

  render() {
    const { classes, children } = this.props;

    return (
      <div className={classes && classes.root}>
        {children}
      </div>
    )
  }
}

Grid_2_1.propTypes = {
  classes: PropTypes.object.isRequired,
  children: PropTypes.array.isRequired
};

export default withStyles(styles)(withRouter(Grid_2_1));
