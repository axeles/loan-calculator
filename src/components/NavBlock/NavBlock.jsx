import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

const styles = theme => ({
  container: {
    width: '100%',
  },
  links: {
    '& a': {
      textDecoration: 'none'
    }
  },
  primaryTextStyle: {
    padding: '.3rem 0',
    textAlign: 'right'
  },
  linkStyle: {
    borderRight: '7px solid',
    paddingRight: '10px'
  },
  activeLink: {
    borderColor: theme.palette.primary.main,
  },
  passiveLink: {
    borderColor: theme.palette.secondary.contrastText
  },
  disabledLink: {
    borderColor: theme.palette.secondary.dark,
    '& span': {
      color: theme.palette.secondary.dark
    }
  },

});

class NavBlock extends Component {

  handleLinkClick(clickedLink) {
    this.props.onLinkSelected(clickedLink);
  }

  buildLinksList = () => {
    const { classes } = this.props;
    const itemStyle = classes && classes.primaryTextStyle;

    return this.props.linksList.map(link => {
      const index = this.props.linksList.indexOf(link);
      let linkStatus;
      switch (this.props.linksList[index].status) {
        case 'active':
          linkStatus = classes && classes.activeLink;
          break;
        case 'passive':
          linkStatus = classes && classes.passiveLink;
          break;
        case 'disabled':
          linkStatus = classes && classes.disabledLink;
          break;
        default:
          linkStatus = classes && classes.passiveLink;
      }

      return (
        <Link to={link.path} onClick={() => this.handleLinkClick(link)} key={link.path}>
          <ListItem button className={itemStyle}>
            <ListItemText
              primary={link.title}
              className={classNames(classes.linkStyle, linkStatus)}
            />
          </ListItem>
        </Link>
      )
    });

  };

  render() {
    // console.log('+++ [NavBlock props]', this.props);
    const { classes } = this.props;
    return (
      <div className={classes && classes.container}>
        <List component="nav" className={ classes && classes.links} >
          {this.buildLinksList()}
        </List>
      </div>
    );
  }
}

NavBlock.propTypes = {
  onLinkSelected: PropTypes.func.isRequired,
  linksList: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(NavBlock);
