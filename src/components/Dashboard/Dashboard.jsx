import React, { Component } from 'react';
import { withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import * as appActions from '../../actions/appActions';
import * as authActions from '../../actions/authActions';
import {Logo} from "flexcom";

const styles = theme => ({
  root: {
    display: 'flex'
  }
});

class Dashboard extends Component {

  onLogoClick = () => {
    this.props.history.push('/');
  };

  render() {
    const { classes } = this.props;
    const { logoParams } = this.props.app;

    return (
        <Logo
          className={classes && classes.root}
          params={logoParams}
          onLogoClick={this.onLogoClick}
        />
    )
  }
}

const mapStateToProps = state => {
  return {
    app: state.appReducer,
    auth: state.authReducer,
    user: state.usersReducer,
    router: state.routerReducer
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginUser: credentials => dispatch(authActions.loginUser(credentials)),
    logoutUser: () => dispatch(authActions.logoutUser()),
    setAnimationClass: payload => dispatch(appActions.setAnimationClass(payload)),
    switchLanguage: payload => dispatch(appActions.switchLanguage(payload)),
    linkSelected: payload => dispatch(appActions.linkSelected(payload)),
    backButtonClicked: payload => dispatch(appActions.backButtonClicked(payload)),
    backButtonChanged: payload => dispatch(appActions.backButtonChanged(payload)),
    getProfileAll: params => dispatch(appActions.getProfileAll(params)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(withRouter(Dashboard)));
