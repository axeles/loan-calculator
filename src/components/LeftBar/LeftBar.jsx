import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from "prop-types";

const styles = theme => ({
  root: {
    display: 'grid',
    gridTemplateColumns: '15% calc(85% - 1rem)',
    gridTemplateRows: '1fr 10rem',
    gridGap: '1rem 1rem',
    fontSize: 'inherit',
    fontFamily: 'inherit',
    backgroundColor: theme.palette.secondary.main
  },
  titleContainer: {
    display: 'grid',
    gridTemplateColumns: 'minmax(min-content, max-content) 6rem',
    gridGap: '1rem 1rem',
    writingMode: 'vertical-rl',
    lineHeight: '1.5rem',
    transform: 'rotate(180deg)',
    justifySelf: 'end',
    alignSelf: 'start'
  },
  title: {
    textAlign: 'right',
    alignSelf: 'center'
  },
  titleName: {
    fontSize: theme.typography.caption.fontSize,
    fontWeight: theme.typography.caption.fontWeight,
    fontFamily: theme.typography.caption.fontFamily,
    textTransform: 'uppercase',
  },
  titleDescription: {
    fontSize: '.815rem',
    color: theme.palette.secondary.main
  },
  titleDecorator: {
    display: 'inline-block',
    backgroundColor: theme.palette.tertiary && theme.palette.tertiary.contrastText,
    width: '2px',
    height: '100%',
    alignSelf: 'end',
    marginTop: '.5rem',
    marginLeft: '.7rem'
  },
  body: {
    justifySelf: 'end',
    marginRight: '3rem'
  },
  footer: {
    backgroundColor: theme.palette.tertiary && theme.palette.tertiary.dark,
    gridColumnStart: 1,
    gridColumnEnd: '-1'
  }
});

class LeftBar extends Component {
  render() {
    const { classes, children, title } = this.props;

    return (
      <div className={classes && classes.root}>
        <div className={classes && classes.titleContainer}>
          <div className={classes && classes.title}>
            <div className={classes && classes.titleDescription}>{title.description}</div>
            <div className={classes && classes.titleName}>{title.name}</div>
          </div>
          <div className={classes && classes.titleDecorator}>&nbsp;</div>
        </div>
        <div className={classes && classes.body}>{children}</div>
        <div className={classes && classes.footer}>&nbsp;</div>
      </div>
    )
  }
}

LeftBar.propTypes = {
  title: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired
  }).isRequired,
  classes: PropTypes.object.isRequired,
  children: PropTypes.array
};

export default withStyles(styles)(withRouter(LeftBar));
