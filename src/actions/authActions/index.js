import { push } from 'react-router-redux';
import * as authConstants from '../../constants/AuthConstant';
import {postData} from "../../utils/FetchUtils";

const requestLogin = credentials => ({
  type: authConstants.AUTH__LOGIN_REQUEST,
  isFetching: true,
  isAuthenticated: false,
  credentials
});

const receivedLogin = () => ({
  type: authConstants.AUTH__LOGIN_SUCCESS,
  isFetching: false,
  isAuthenticated: true
});

const loginError = message => ({
  type: authConstants.AUTH__LOGIN_FAILURE,
  isFetching: false,
  isAuthenticated: false,
  message
});

export const loginUser = credentials => {
  const creds = {
    email: credentials.login,
    password: credentials.password
  };
  return dispatch => {
    dispatch(requestLogin(creds));
    postData(authConstants.AUTH__AUTH_URL, creds)
      .then(response => {
        // console.log('+++ response', response); // JSON-string from `response.json()` call
        if (response.token) {
          localStorage.setItem('token', response.token);
          dispatch(receivedLogin(response.token));
          // dispatch(push('/'));
          // console.log('+++ User logged in. Got token:', response.token);
        }
      })
      .catch(e => dispatch(loginError(e.toString())));
  }
};

// Log out the user
const requestLogout = () => ({
  type: authConstants.AUTH__LOGOUT_REQUEST,
  isFetching: true,
  isAuthenticated: true
});

const receivedLogout = () => ({
  type: authConstants.AUTH__LOGOUT_SUCCESS,
  isFetching: false,
  isAuthenticated: false
});

export const logoutUser = () => dispatch => {
  dispatch(requestLogout());
  localStorage.removeItem('token');
  dispatch(receivedLogout());
};

