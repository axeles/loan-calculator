import { isFirefoxOrIE } from '../utils/';
import * as consts from '../constants/UsersConstants';

export const getResponseBody = response => {
  if (response.status === 200) {
    if (isFirefoxOrIE && !response.bodyUsed) {
      return response.blob().then(myBlob => myBlob);
    }

    return response.body;
  }
  return response;
};

export const error = message => ({
  type: consts.FAIL,
  message
});
