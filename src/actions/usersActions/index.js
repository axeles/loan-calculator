import * as usersConstants from '../../constants/UsersConstants';
import { APP_URL } from '../../constants/urls';

import { getJSONHeader } from '../../utils/';
import { error } from '../utils';

const getAllUsers = (page, count) => {
  const config = {
    method: 'GET',
    headers: getJSONHeader()
  };
  const url = `${APP_URL}${
    usersConstants.USERS__GET_USERS_URL
  }?page=${page}&count=${count}`;

  return dispatch =>
    fetch(url, config)
      .then(response => {
        response.json().then(userList =>
          dispatch({
            type: usersConstants.USERS__GET_USERS,
            page,
            count,
            userList
          })
        );
      })
      .catch(e => {
        dispatch(error(e));
      });
};

const setRowsPerPage = count => dispatch =>
  dispatch({ type: usersConstants.USERS__SET_ROWS_PER_PAGE, count });

const setPage = page => dispatch =>
  dispatch({ type: usersConstants.USERS__SET_PAGE, page });

const setFilter = filter => dispatch =>
  dispatch({ type: usersConstants.USERS__SET_FILER, filter });

const editUser = (newFlag, user, userId) => {
  const mainBody = {
    name: user.name,
    surname: user.surname,
    patronymic: user.patronymic,
    passportSeries: user.passportSeries,
    passportNumber: user.passportNumber,
    birthday: user.birthday,
    position: user.position,
    location: {
      id: user.location
    },
    appointmentDate: user.appointmentDate,
    user: {
      login: user.login,
      password: user.password,
      role: user.position,
      locationId: user.location,
      active: user.active
    },
    active: user.active
  };

  let body = {
    id: user.id,
    ...mainBody
  };
  body.user = { id: user.id, ...mainBody.user };

  const config = {
    method: newFlag ? 'POST' : 'PUT',
    headers: getJSONHeader(),
    body: JSON.stringify(newFlag ? mainBody : body)
  };

  const url = newFlag
    ? `${APP_URL}${usersConstants.USERS__EDIT_USER_URL}/add`
    : `${APP_URL}${usersConstants.USERS__EDIT_USER_URL}/update`;

  return (dispatch, getState) => {
    const { page, count } = getState().usersReducer;
    return fetch(url, config)
      .then(resp => {
        dispatch({
          type: usersConstants.USERS__EDIT_USER,
          resp
        }),
          dispatch(getAllUsers(page, count));
      })
      .catch(e => {
        dispatch(error(e));
      });
  };
};
export default { getAllUsers, setRowsPerPage, setPage, setFilter, editUser };
