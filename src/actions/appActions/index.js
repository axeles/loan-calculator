import * as appConstants from '../../constants/AppConstant';
import { APP_URL } from '../../constants/urls';
import {getData} from "../../utils/FetchUtils";

export const linkSelected = (payload) => {
  return {
    type: appConstants.APP__LINK_SELECTED,
    payload
  };
};

export const backButtonClicked = (payload) => {
  return {
    type: appConstants.APP__BACK_BUTTON_CLICKED,
    payload
  };
};

export const backButtonChanged = (payload) => {
  return {
    type: appConstants.APP__BACK_BUTTON_CHANGED,
    payload
  };
};

// Search methods
const requestSearch = () => ({
  type: appConstants.APP__SEARCH_REQUEST,
  isFetching: true
});

const receivedSearchResults = (profiles) => ({
  type: appConstants.APP__SEARCH_SUCCESS,
  isFetching: false,
  showSearchResults: true,
  showSearchInSidebar: true,
  profiles
});

const searchError = message => ({
  type: appConstants.APP__SEARCH_FAILURE,
  isFetching: false,
  showSearchResults: false,
  message
});

const searchParams = params => ({
  type: appConstants.APP__SEARCH_PARAMS,
  params
});

export const getProfileAll = (params) => {
  const url = APP_URL + '/api/profile/all';

  return dispatch => {
    dispatch(requestSearch());
    dispatch(searchParams(params));
    getData(url)
      .then(result => {
        // console.log('+++ result of fetch', result);
        dispatch(receivedSearchResults(result));
      })
      .catch(e => dispatch(searchError(e.toString())));
  }
};

export const setAnimationClass = (payload) => {
  return {
    type: appConstants.APP__SET_ANIMATION_CLASS,
    payload
  };
};

export const switchLanguage = (payload) => {
  return {
    type: appConstants.APP__SWITCH_LANGUAGE,
    payload
  };
};


